from abc import ABC

from sqlalchemy import Column, Integer, ForeignKey, String
from sqlalchemy.orm import relationship

from app import db

EQUIPMENT_TYPES_TABLE_NAME = "equipment_types"
COUNTERS_TABLE_NAME = "daily_station_equipment_counters"
STATIONS_TABLE_NAME = "stations"
ORDERS_TABLE_NAME = "orders"
ORDER_EQUIPMENT_COUNTER_TABLE_NAME = "order_equipment_counters"


def generate_date_code_column() -> Column:
    return Column(String(8))


class HavingId:
    ID_COLUMN_NAME = 'id'
    id = Column(Integer, primary_key=True)


class HavingName:
    name = Column(String)


class AbstractDailyStationEquipmentCounter(db.Model, HavingId):
    __tablename__ = COUNTERS_TABLE_NAME

    discriminator = Column(String(7))

    equipment_type_id = Column(
        Integer,
        ForeignKey(EQUIPMENT_TYPES_TABLE_NAME + "." + HavingId.ID_COLUMN_NAME)
    )

    station_id = Column(
        Integer,
        ForeignKey(STATIONS_TABLE_NAME + "." + HavingId.ID_COLUMN_NAME)
    )

    count = Column(Integer)
    day_code = generate_date_code_column()

    __mapper_args__ = {
        "polymorphic_on": discriminator
    }


class OnHandCounter(AbstractDailyStationEquipmentCounter):
    __mapper_args__ = {
        "polymorphic_identity": "onhand"
    }


class BookedCounter(AbstractDailyStationEquipmentCounter):
    __mapper_args__ = {
        "polymorphic_identity": "booked"
    }


class EquipmentType(db.Model, HavingId, HavingName):
    __tablename__ = EQUIPMENT_TYPES_TABLE_NAME


class Station(db.Model, HavingId, HavingName):
    __tablename__ = STATIONS_TABLE_NAME


class OrderEquipmentCounter(db.Model, HavingId):
    __tablename__ = ORDER_EQUIPMENT_COUNTER_TABLE_NAME
    order_id = Column(Integer, ForeignKey(ORDERS_TABLE_NAME + "." + HavingId.ID_COLUMN_NAME))
    order: "Order"


class Order(db.Model, HavingId):
    __tablename__ = ORDERS_TABLE_NAME
    order_equipment_counters = relationship(OrderEquipmentCounter, backref="order", uselist=True)

    start_day_code = generate_date_code_column()
    end_day_code = generate_date_code_column()

    start_station_id = Column(Integer, ForeignKey(STATIONS_TABLE_NAME + "." + HavingId.ID_COLUMN_NAME))
    start_station = relationship("Station", foreign_keys=[start_station_id])

    end_station_id = Column(Integer, ForeignKey(STATIONS_TABLE_NAME + "." + HavingId.ID_COLUMN_NAME))
    end_station = relationship("Station", foreign_keys=[end_station_id])
