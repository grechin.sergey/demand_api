from app import db
from app.mod_core.models import Station, Order, OrderEquipmentCounter, EquipmentType

db.session.query(Station).delete()
db.session.query(EquipmentType).delete()

berlin = Station()
berlin.name = "Berlin"

frankfurt = Station()
frankfurt.name = "Frankfurt"

towel = EquipmentType()
towel.name = "Towel"

tooth_brush = EquipmentType()
tooth_brush.name = "Tooth brush"


db.session.add_all([berlin, frankfurt, towel, tooth_brush])
db.session.commit()
